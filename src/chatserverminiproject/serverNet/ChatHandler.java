package chatserverminiproject.serverNet;

import java.util.ArrayList;

import chatminiproject.comNet.Container;

public class ChatHandler {
	
	private static ChatHandler cH;
	
	private ArrayList<ChatRoom> chatRooms;
	
	
	
	
	private ChatHandler() {
		this.chatRooms = new ArrayList<>();
	}
	

	public static ChatHandler  getCH() {
		if(cH == null) {
			cH = new ChatHandler();
		}
		return cH;
	}


	public void processMessage(ClientThread senderCT, Container c ) {  
		
		String information = c.getInfomration();
		String username = c.getUsername();
		String chatRoomiD = c.getDestination();
		
		
		
		int iD = Integer.parseInt( chatRoomiD);
	 ChatRoom cR = this.findChatRoom(iD);
	 
	 cR.postMessage(information);
		
		// todo process message completely
		
	}
	
	
	public void processOpenChatRoom(String chatRoomName) {
		
		ChatRoom cR = new ChatRoom(chatRoomName);
		
		this.chatRooms.add(cR);
		
		
		
		Container c = new Container("server", "appendChatRoom");
		
		ClientHandler.broadcast(c);
		
	}
	
	
	
	

	private ChatRoom findChatRoom(int iD) {
		
		ChatRoom cR = null;
		
		for(ChatRoom cRr: this.chatRooms) {
			
			if(cRr.getID() == iD) {
				cR = cRr;
				break;
			}
			
		}
		
		return cR;
	}

	public ArrayList<String> getAllRooms(ClientThread cT) {
		
		ArrayList<String> rooms = new ArrayList<>();
		
		for(ChatRoom cR: this.chatRooms) {
			
			if(cR.clientIsinChatRoom(cT)) {
				
				rooms.add(cR.stringRep());
			}
			
			
		}
		
		
		return rooms;
	}


	public ArrayList<String> getAllChatHist() {
		// TODO Auto-generated method stub
		return null;
	}

	
	
	
	
	
	
	
	
	
}
