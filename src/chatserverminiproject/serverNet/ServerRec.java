package chatserverminiproject.serverNet;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import chatminiproject.ServiceLocator;
import chatminiproject.comNet.Container;

public class ServerRec extends Thread {

	private static ServerRec sR;

	private ServerSocket sSocket;

	
	//TODO: make dynamic alter
	private int port = 28000;

	private ClientHandler clientHandler;

	private int counter;

	private volatile boolean run = true;
	private ServiceLocator sL;
	
	
	private ServerRec() {
	
		this.sL =ServiceLocator.getServiceLocator();
		
		
	}
	
	
	
	public static ServerRec getSR() {
		if(sR == null) {
			sR = new ServerRec();
		}
		return sR;
	}
	
    private void resetSingletonObject() {
		sR = null;
		ServerRec.getSR();
	}
	
	public void startServerSocket() {
		
		this.startSSocket();
	}
	
	
	
	public void startServerSocket(int port, ClientHandler clientHandler) {
		this.port = port;
		this.clientHandler = clientHandler;
		
		this.startSSocket();
		
	}
	
	
	private void startSSocket() {
		this.setDaemon(true);
		this.start();
	    
		this.run = true;
		
		
	}
	
	
	
	
	
	public void run() {
		
		
		
		
		
		try {
			this.sSocket = new ServerSocket(this.port, 10);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	
		
		sL.getLogger().info("server started");
		
		
		
		
		while(run) {
			
			try {
				
				
			sL.getLogger().info("is listening");
				
				
				Socket socket = sSocket.accept();
				
			
				
				sL.getLogger().info("client accepted");
				
				ClientThread cT = new ClientThread(socket);
				
				
				cT.setClientHandler(this.clientHandler);
				this.clientHandler.addClientThread(cT);
				
				
				
	
				cT.start();
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				// to ensure a close 
				if(!run) {
					try {
						this.sSocket.close();
						
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			
			
			
			
			
			
		}
		
		
		
		
		
	}



	private void testCommand() {

		this.counter++;
		System.out.println(counter);
		
		if(this.counter == 4) {
			
			sL.getLogger().info("killing all clients");
			
			Container c = new Container("server", "shutdown");
			
			
			ClientHandler.broadcast(c);
		}
		
		
	}



	public void closeConnection() {
		this.run = false;
		
		//Container an client senden
		
		
			try {
				sSocket.close();
				
				sL.getLogger().info("serverSocket closed");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
			this.resetSingletonObject();
		
			//close all ClientThreads
			ClientHandler.closeAllClientConnections();
			
	}
	
	
	public boolean isConnected() {
		
		boolean isOn;
		
		if (this.sSocket == null) {
			isOn = false;
		}else {
			if (this.sSocket.isClosed()) {
				isOn = false;
			
		}else {
			isOn = true;
		}
		}

	//System.out.println(isOn);
		return isOn;
	}
	
	
	
}
