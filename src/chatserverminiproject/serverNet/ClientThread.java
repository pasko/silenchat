package chatserverminiproject.serverNet;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketException;

import chatminiproject.ServiceLocator;
import chatminiproject.comNet.Container;

public class ClientThread extends Thread {
	
	
	
	
	 	private Socket cSocket;
	    
	    private ObjectInputStream inC;
	    private ObjectOutputStream outC;
	    

		private int iD;

		private ClientHandler clientHandler;

	public ClientThread(Socket cSocket) {
		 this.cSocket = cSocket;
		
		
		   
		  try {
		this.inC = new ObjectInputStream(this.cSocket.getInputStream());
			this.outC = new ObjectOutputStream(this.cSocket.getOutputStream());
			
			this.setDaemon(true);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			
			System.out.println("Exception: ");
			e.printStackTrace();
		}
		   
		  
		  
	
	}

	
	  
	   public synchronized void send(Container c) {
		   
		   try {
			this.outC.writeObject(c);
			this.outC.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	   }
	   
	
	public void run() {
		
		while(true) {
			
			try {
				Container c =  (Container) inC.readObject();
				
				ServiceLocator.getServiceLocator().getLogger().info("receiving: " + c.stringRep());
               this.clientHandler.processContainer(c, this);
				
              
				
			} catch(SocketException sE) {
				sE.printStackTrace();
				System.out.println("SocketException from Client " + this.iD);
				this.closeClientConnection();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			
			
			
		}
	
	
	
	}



	public void setID(int iD) {
		this.iD = iD;
		
	}



	public int getiD() {
		return iD;
	}



	public void setClientHandler(ClientHandler clientHandler) {
		this.clientHandler = clientHandler;
		
	}
	
	public void closeClientConnection() {
		
		try {
			this.outC.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			this.inC.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			this.cSocket.getInputStream().close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			this.cSocket.getOutputStream().close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		try {
			this.cSocket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}



	public String getUserName() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}
	
	
	
	

