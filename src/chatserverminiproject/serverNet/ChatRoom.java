package chatserverminiproject.serverNet;

import java.util.ArrayList;

import chatminiproject.comNet.Container;

public class ChatRoom {

	
	
	
	private ArrayList<String> messages;
	private ArrayList<ClientThread> cTs;
	private String name;
	private int iD;
	
	private static int counter;
	
	public ChatRoom(String name) {
		this.cTs = new ArrayList<>();
		this.name = name;
		
		counter++;
		this.iD = counter;
		
		
	}
	
	
	public void postMessage(String message) {
		
		this.messages.add(message);
		
		Container c = new Container("server", "message" , message, Integer.toString(this.iD) );
		this.broadcast(c);
		
		
		
	}
	
	public int getID() {
		return this.iD;
	}
	
	
	public boolean clientIsinChatRoom(ClientThread cT) {
		     
	  for(ClientThread cTc :this.cTs) {
		  if(cTc.getiD() == cT.getiD()) {
			  
			  return true;
			  
		  }
	  }
		
		return false;
		
	}
	
	
	private void broadcast(Container c) {
		
		for(ClientThread cT: this.cTs) {
			cT.send(c);
		}
		
	}


	public String stringRep() {
		
		String rep = this.name + ";" +  this.iD;
		
		
		return null;
	}
	
}
