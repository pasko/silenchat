package chatserverminiproject.serverNet;

import java.util.ArrayList;

import chatminiproject.ServiceLocator;
import chatminiproject.comNet.Container;

public class ClientHandler {
	
	
	private static int idCounter = 0;
	private static ArrayList<ClientThread> clients;
	private static ArrayList<User> users;
	
	private static ServiceLocator sL;
	
	
	public ClientHandler() {
		clients = new ArrayList<ClientThread>();
		this.users = new ArrayList<User>();
		this.sL = ServiceLocator.getServiceLocator();
		
	}
	
	
	public void addClientThread(ClientThread cT) {
		
		
		//control if a client already was on the server, and give him his old id instead of the counter
		
		
		clients.add(cT);

		
		
		
		
		
	}
	
	private void processBrief(ClientThread cT, Container c ) {
		
		
		
		String userName = c.getUsername();
		
		
		
		if(this.controllUser(userName)) {
			
		User u = this.findUser(userName);
			
		cT.setID(u.getiD());	
			
			
			
			
			
		} else {
			
		
		this.sL.getLogger().info("user does not exist, creating new");
			
			
	    cT.setID(idCounter);
		idCounter++;
		
		
		User u = new User(userName);
		u.setiD(cT.getiD());
		
		
		this.users.add(u);
		
		this.sL.getLogger().info("user created with iD: " + u.getiD() + " and userName: " + u.getUserName());
		}
		
		
		
		
		
		
		this.firstContact(cT);
		
		
		
		
	}
	

	private User findUser(String userName) {
		
		User us = null;
		
		for(User u: this.users) {
			
			if(u.getUserName().equals(userName)) {
				
				us = u;
			}
			
			
			
		}
		
		return us;
		
	}


	private boolean controllUser(String userName) {
		
		boolean userExists = false;
		
		for (User u : this.users) {
			
			
			if(u.getUserName().equals(userName)) {
				userExists = true;
				return userExists;
			}
			
			
			
		}
		
		
		
		return false;
	}


	private void firstContact(ClientThread cT) {
		
		
		
		
		Container c = new Container("server", "firstContact", ChatHandler.getCH().getAllRooms(cT), ChatHandler.getCH().getAllChatHist(), this.getAllContacts(), cT.getiD());
		
		cT.send(c);
		
		
		
	}


	private ArrayList<String> getAllContacts() {
		
		
		ArrayList<String> contacts = new ArrayList<>();
		
		for(ClientThread cT: clients) {
			
			contacts.add(cT.getUserName());
			
			
			
		}
		
		
		
		return contacts;
	}


	public synchronized void processContainer(Container c, ClientThread sendercT) {
	
		
		String dest = c.getDestination();
	
	
	
		switch(c.getCommand()) {
		
		
		case "brief" : this.processBrief(sendercT, c);
		
		case "openChatRoom": ChatHandler.getCH().processOpenChatRoom(c.getInfomration());
		
		
		
//		case "message": ChatHandler.getCH().processMessage(c.getInfomration(), c.getUsername(), sendercT, findDestClient(dest) );
		
		
		
		
		
		
		}
		
		
		
		
	}
	
	
	
	private ClientThread findDestClient(String dest) {
		
		
		int des = Integer.parseInt(dest);
		
		
		ClientThread cT = findClient(des);
		if(cT == null) {
			sL.getLogger().info("could not find client");
		}
		
		
		return cT;
	}
	
	
	
	
	public static ClientThread findClient(int iD) {
		
	ClientThread cTS = null;
		
		for(ClientThread cT : clients) {
			if(cT.getiD() == iD) {
				cTS = cT;
				break;
			}
		}
		
		return cTS;
	}
	
	
	public static void broadcast(Container c) {
		for(ClientThread  cT : clients) {
			cT.send(c);
		}
		
	}
	
	public static void closeAllClientConnections() {
		
		for(ClientThread cT: clients) {
			cT.closeClientConnection();
		}
		
	 clients.clear();
	 
	 
		
		sL.getLogger().info("all clientconnections closed");
	}
	
	

}
