package chatserverminiproject.serverApp;

import java.text.SimpleDateFormat;

import com.sun.jmx.snmp.Timestamp;

import chatminiproject.ServiceLocator;
import chatserverminiproject.serverNet.ClientHandler;
import chatserverminiproject.serverNet.ServerRec;
import javafx.application.Platform;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.concurrent.Task;
import javafx.scene.paint.Color;
import javafx.stage.WindowEvent;
import sun.awt.SunHints.Value;
import sun.rmi.log.LogHandler;

public class ServerController extends Thread {
	private ServerView view;
	private ServerModel model;
	ServiceLocator sL;
	private int port;
	public static volatile boolean status = false;
	private ClientHandler clientHandler;
	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");


	public ServerController(ServerModel model, ServerView view) {
		this.view = view;
		this.model= model;
		
		this.clientHandler = new ClientHandler();
		this.sL = ServiceLocator.getServiceLocator();
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		
		
		
		
		view.getStartUp().setOnAction(e->{
			
			if (!ServerRec.getSR().isConnected()) {
				startServer();

				
				sL.getLogger().info("open port: " + this.port);
				
				status = true;
			}else {
				
				
				sL.getLogger().info("server should disconnect");
				closeServer();
				this.status = false;
			}
			
			
			
			
			model.startStop();
		});
		
		model.getStatus().addListener((observable, oldStatus, newStatus)->{

			updateGUI(newStatus);
		});
		

		
		view.getStage().setOnCloseRequest(e-> {
			view.stop();
			
			Platform.exit();
		});
		
		
    LogViewHandler.getRecString().addListener((o,old,n)->{
    	
    	view.getTxtInfo().appendText(" >> " +n + "\n");
    	
    });

	}
	
	


	
	
	private void closeServer() {
		ServerRec.getSR().closeConnection();
		
	}

	

	private void startServer() {
		
		port = Integer.parseInt(view.getPort().getText());		
		ServerRec.getSR().startServerSocket(port, this.clientHandler);
//TODO: mit Silen anschauen		
	sL = ServiceLocator.getServiceLocator();
	sL.getLogger().info("Server Connected");

		
		
	}
	
	/**
	 * 
	 * 
	 * Watch out there are some bugs around
	 * 
	 * 
	 * 
	 * @author pascal
	 */

	
	private void updateGUI(String newStatus) {
		Platform.runLater(() -> {
			view.status.setText(newStatus);
			view.status.setTextFill(Color.GREEN);
			//view.getStartUp().setText(ServerRec.getSR().isConnected() ? "Disconnect" : "Connect");
		});
		
	}

}
