package chatserverminiproject.serverApp;

import java.util.logging.LogRecord;
import java.util.logging.StreamHandler;

import javafx.beans.property.SimpleStringProperty;

public class LogViewHandler extends StreamHandler{
	
	private static SimpleStringProperty recString = new SimpleStringProperty();
	
	
	
	public void publish(LogRecord rec) {
		
		recString.setValue(rec.getMessage());
		
		
		super.publish(rec);
	}
	
	
	
	@Override
    public void flush() {
        super.flush();
    }


    @Override
    public void close() throws SecurityException {
        super.close();
    }



	public static SimpleStringProperty getRecString() {
		return recString;
	}



	public static void setRecString(SimpleStringProperty recString) {
		LogViewHandler.recString = recString;
	}
	
	
	

}
