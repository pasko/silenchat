package chatserverminiproject.serverApp;

import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import chatminiproject.ServiceLocator;
import chatminiproject.commonClasses.Configuration;
import chatserverminiproject.serverNet.ClientHandler;
import chatserverminiproject.serverNet.ServerRec;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;



public class Main extends Application {
	

	private ServerView view;
	private ServerModel model;
	private ServerController controller;
	
	private static ServiceLocator sL;

	public static void main(String[] args) {
		
		
		
		   // Create the service locator to hold our resources
       sL = ServiceLocator.getServiceLocator();
      

        // Initialize the resources in the service locator
       sL.setLogger(configureLogging());


        sL.setConfiguration(new Configuration());
       

       
		
		launch(args);
		
		//ServerRec.getSR().startServerSocket(28000, new ClientHandler());

		
	}
	
	


	
	public void start(Stage serverStage) throws Exception {
		
		this.model = new ServerModel();
		this.view = new ServerView(serverStage, model);
		this.controller = new ServerController(model, view);
		
		
		view.start();
		


	}
	

	
	 private static Logger configureLogging() {
	        Logger rootLogger = Logger.getLogger("");
	        rootLogger.setLevel(Level.FINEST);

	        // By default there is one handler: the console
	        Handler[] defaultHandlers = Logger.getLogger("").getHandlers();
	        defaultHandlers[0].setLevel(Level.INFO);

	        // Add our logger
	        Logger ourLogger = Logger.getLogger(sL.getAPP_NAME());
	        ourLogger.setLevel(Level.FINEST);
	        
	        // Add a file handler, putting the rotating files in the tmp directory
	        try {
	            Handler logHandler = new FileHandler("%t/"
	                    + sL.getAPP_NAME() + "_%u" + "_%g" + ".log",
	                    1000000, 9);
	            logHandler.setLevel(Level.FINEST);
	            
	            
	         LogViewHandler lVH =  new LogViewHandler();
	            lVH.setLevel(Level.FINEST);
	            
	            ourLogger.addHandler(logHandler);
	            ourLogger.addHandler(lVH);
	        } catch (Exception e) { // If we are unable to create log files
	            throw new RuntimeException("Unable to initialize log files: "
	                    + e.toString());
	        }

	        return ourLogger;
	    }
	

}
