package chatserverminiproject.serverApp;


import chatminiproject.ServiceLocator;
import chatminiproject.abstractClasses.View;
import chatserverminiproject.serverNet.ServerRec;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;

public class ServerModel {

	

	
	private SimpleIntegerProperty value;
	private SimpleStringProperty serverStatus;
	private volatile boolean stop = true;


	public class OurTask extends Task<Void> {

		@Override
		protected Void call() throws Exception {
			while (!stop) {
				
				ServerRec.getSR().isConnected();
				
				if(ServerController.status =true) {
					serverStatus.set("online");
					//System.out.println(ServerController.status);
				}else {
					serverStatus.set("offline");
					//System.out.println(ServerController.status);
				}
					

				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {

				}
			}
			return null;
		}

	}

	protected ServerModel() {

		serverStatus = new SimpleStringProperty("offline");
	}

	public SimpleStringProperty getStatus() {
		return serverStatus;
	}



	public void startStop() {
		if (stop) {
			stop = false;
			OurTask task = new OurTask();
			new Thread(task, "Server is online and running").start();
		} else {
			stop = true;
		}
	}

}
