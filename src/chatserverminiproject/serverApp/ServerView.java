package chatserverminiproject.serverApp;

import java.util.Locale;
import java.util.logging.Logger;

import com.sun.security.ntlm.Client;

import chatminiproject.ServiceLocator;
import chatminiproject.commonClasses.Translator;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 * 
 * 
 * 
 * 
 * @author pascal
 * 
 */

public class ServerView  {

	public Stage getStage() {
		return stage;
	}

	public void setStage(Stage stage) {
		this.stage = stage;
	}

	Menu menuFile;
	Menu menuFileLanguage;
	Menu menuHelp;
	

	Label status;
	private Label lblStatus, lblPort, lblIp;

	private Button startUp;

	private Stage stage;
	private ServerModel model;
	private BorderPane root;
	private HBox statusView, buttonView, detailView;
	private VBox centerV;
	private TextField ip, port;
	private TextArea txtInfo = new TextArea();
	

	public TextArea getTxtInfo() {
		return txtInfo;
	}

	public void setTxtInfo(TextArea txtInfo) {
		this.txtInfo = txtInfo;
	}

	public ServerView(Stage stage, ServerModel model) {
		this.model = model;
		this.stage = stage;



		root = new BorderPane();
		status = new Label();
		
		status.setText(model.getStatus().get());
		status.setTextFill(Color.RED);
		
		
		lblStatus = new Label("Status: ");
		lblPort = new Label("Port: ");
		lblIp = new Label("Ip: ");
		startUp = new Button();
		statusView = new HBox(10);
		buttonView = new HBox(10);
		centerV = new VBox(10);
		startUp = new Button("Connect");
		detailView = new HBox(10);
		ip = new TextField("127.0.0.1");
		port = new TextField ("28000");
		

		statusView.getChildren().addAll(lblStatus, status);
		statusView.setAlignment(Pos.CENTER);
		
		detailView.getChildren().addAll(lblIp, ip, lblPort, port);
		detailView.setAlignment(Pos.CENTER);
		
		buttonView.getChildren().addAll(startUp);
		buttonView.setAlignment(Pos.CENTER);
		buttonView.setPadding(new Insets(10, 10, 10, 10));
		

		centerV.getChildren().addAll(statusView, detailView);
		centerV.setAlignment(Pos.CENTER);

		root.setTop(centerV);
		
		//TODO: Paddings refractor
		root.setCenter(txtInfo);
		/*
		 * Read only TextArea 
		 * 
		 * Inspiration from StackOverflow
		 */
		
		txtInfo.setEditable(false);
		txtInfo.setMouseTransparent(true);
		txtInfo.setFocusTraversable(false);
		txtInfo.setPadding(new Insets(5,5,5,5));
		
		
		root.setBottom(buttonView);
		

		
		Scene scene = new Scene (root, 450, 600);
		
		scene.getStylesheets().add(getClass().getResource("stylesheet.css").toExternalForm());
		stage.setScene(scene);
		stage.setTitle("Server StartUp");
		

	}




	public VBox getCenterV() {
		return centerV;
	}

	public void setCenterV(VBox centerV) {
		this.centerV = centerV;
	}

	public TextField getIp() {
		return ip;
	}

	public void setIp(TextField ip) {
		this.ip = ip;
	}

	public TextField getPort() {
		return port;
	}

	public void setPort(TextField port) {
		this.port = port;
	}

	public Menu getMenuFile() {
		return menuFile;
	}

	public void setMenuFile(Menu menuFile) {
		this.menuFile = menuFile;
	}

	public Menu getMenuFileLanguage() {
		return menuFileLanguage;
	}

	public void setMenuFileLanguage(Menu menuFileLanguage) {
		this.menuFileLanguage = menuFileLanguage;
	}

	public Menu getMenuHelp() {
		return menuHelp;
	}

	public void setMenuHelp(Menu menuHelp) {
		this.menuHelp = menuHelp;
	}

	public Label getStatus() {
		return status;
	}

	public void setStatus(Label status) {
		this.status = status;
	}

	public Label getLblStatus() {
		return lblStatus;
	}

	public void setLblStatus(Label lblStatus) {
		this.lblStatus = lblStatus;
	}

	public Button getStartUp() {
		return startUp;
	}

	public void setStartUp(Button startUp) {
		this.startUp = startUp;
	}

	public BorderPane getRoot() {
		return root;
	}

	public void setRoot(BorderPane root) {
		this.root = root;
	}

	public HBox getStatusView() {
		return statusView;
	}

	public void setStatusView(HBox statusView) {
		this.statusView = statusView;
	}

	public HBox getButtonView() {
		return buttonView;
	}

	public void setButtonView(HBox buttonView) {
		this.buttonView = buttonView;
	}

	public void start() {
		stage.show();
		
	}
	
	public void stop() {
		stage.hide();
	}
	
//	protected void updateClients() {
//		StringBuffer sb = new StringBuffer();
//		for (Client c : model.clients) {
//			sb.append(c.toString());
//			sb.append("\n");
//		}
//		txtInfo.setText(sb.toString());
//	}

}
