package chatminiproject.appClasses.views;

import java.util.ArrayList;

import javafx.scene.control.ScrollPane;
import javafx.scene.layout.VBox;

public class ChatroomOverView extends ScrollPane {

	
	
	private VBox holder;
	
	public ChatroomOverView() {
		
		this.holder = new VBox();
		
		this.setContent(holder);
		
		
	}
	
	
	public void populateHolder(ArrayList<ChatRoomTeaser> teasers) {
		
		this.holder.getChildren().clear();
		this.holder.getChildren().addAll(teasers);
		
		
		
		
	}
	
	public void appendTeaser(ChatRoomTeaser teaser) {
		
		this.holder.getChildren().add(teaser);
		
		
	}
	
	
	
	
	
	
	
	
	
	
}
