package chatminiproject.appClasses.views;

import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

public class ChatRoomTeaser extends VBox {

	private String chatRoomName;
	private int iD;
	
	private Label nameLabel;
	
	
	public ChatRoomTeaser(String chatRoomName, String s) {
		this.chatRoomName = chatRoomName;
		
		this.iD = Integer.parseInt(s);
		
		
		this.nameLabel = new Label(this.chatRoomName);
		
		
		
		this.nameLabel.setOnMouseClicked(e->{
			
			ChatRoomTeaserTrigger.getCRTT().trigger(this.iD);
			
			
		});
	}
	
	
	
	
	
	
	
	
	
	
	
	
}
