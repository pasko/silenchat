package chatminiproject.appClasses.views;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class LoginBoxView extends VBox{

	
	private HBox headBox;
	private HBox userNameBox;
	private HBox serverBox;
	private Button btnConnect;
	private TextField userNameInput;
	private TextField ipInput;
	private TextField portInput;
	private Label heading;
	private Label userName;
	private Label ipLbl;
	private Label portLbl;
	private VBox loginBox;

	
	
	public LoginBoxView() {
		
		
		
		
		
		
		btnConnect = new Button ();
		
		
		
		userNameInput = new TextField();
		ipInput = new TextField();
		portInput = new TextField();
		
		
		
		
		headBox = new HBox(10);
		userNameBox = new HBox(10);
		serverBox = new HBox(10);
		
		heading =  new Label();
		userName =  new Label();
		ipLbl =  new Label();
		portLbl =  new Label();
		
		
		
		headBox.getChildren().addAll(heading);
		headBox.setAlignment(Pos.TOP_CENTER);
		
		userNameBox.getChildren().addAll(userName, userNameInput, btnConnect);
		userNameBox.setAlignment(Pos.CENTER);
		serverBox.getChildren().addAll(ipLbl, ipInput , portLbl, portInput);
		serverBox.setAlignment(Pos.CENTER);
		
		
		
		loginBox = new VBox(10);
		
		
		
		loginBox.getChildren().addAll(headBox, userNameBox, serverBox);
		loginBox.setAlignment(Pos.CENTER);
		
		this.getChildren().add(loginBox);
	}



	public HBox getHeadBox() {
		return headBox;
	}



	public void setHeadBox(HBox headBox) {
		this.headBox = headBox;
	}



	public HBox getUserNameBox() {
		return userNameBox;
	}



	public void setUserNameBox(HBox userNameBox) {
		this.userNameBox = userNameBox;
	}



	public HBox getServerBox() {
		return serverBox;
	}



	public void setServerBox(HBox serverBox) {
		this.serverBox = serverBox;
	}



	public Button getBtnConnect() {
		return btnConnect;
	}



	public void setBtnConnect(Button btnConnect) {
		this.btnConnect = btnConnect;
	}



	public TextField getUserNameInput() {
		return userNameInput;
	}



	public void setUserNameInput(TextField userNameInput) {
		this.userNameInput = userNameInput;
	}



	public TextField getIpInput() {
		return ipInput;
	}



	public void setIpInput(TextField ipInput) {
		this.ipInput = ipInput;
	}



	public TextField getPortInput() {
		return portInput;
	}



	public void setPortInput(TextField portInput) {
		this.portInput = portInput;
	}



	public Label getHeading() {
		return heading;
	}



	public void setHeading(Label heading) {
		this.heading = heading;
	}



	public Label getUserName() {
		return userName;
	}



	public void setUserName(Label userName) {
		this.userName = userName;
	}



	public Label getIpLbl() {
		return ipLbl;
	}



	public void setIpLbl(Label ipLbl) {
		this.ipLbl = ipLbl;
	}



	public Label getPortLbl() {
		return portLbl;
	}



	public void setPortLbl(Label portLbl) {
		this.portLbl = portLbl;
	}



	public VBox getLoginBox() {
		return loginBox;
	}



	public void setLoginBox(VBox loginBox) {
		this.loginBox = loginBox;
	}
	
	
	
}
