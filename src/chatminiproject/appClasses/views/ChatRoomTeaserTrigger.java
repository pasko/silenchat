package chatminiproject.appClasses.views;

import javafx.beans.property.SimpleIntegerProperty;

public class ChatRoomTeaserTrigger extends SimpleIntegerProperty {

	
	private static ChatRoomTeaserTrigger cRTT;
	
	
	private ChatRoomTeaserTrigger() {
		
	}
	
	public static ChatRoomTeaserTrigger getCRTT() {
		
		if(cRTT == null) {
			cRTT = new ChatRoomTeaserTrigger();
		}
		
	return cRTT;	
	}
	
	
	public void trigger(int iD) {
		
		this.setValue(iD);
		
	}
	
	
	
	
	
}
