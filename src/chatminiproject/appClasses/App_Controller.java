package chatminiproject.appClasses;

import java.util.ArrayList;

import chatminiproject.ServiceLocator;
import chatminiproject.abstractClasses.Controller;
import chatminiproject.appClasses.views.ChatRoomTeaser;
import chatminiproject.appClasses.views.ChatRoomTeaserTrigger;
import chatminiproject.comNet.Container;
import chatminiproject.comNet.NetRecSend;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.WindowEvent;

/**
 * Copyright 2015, FHNW, Prof. Dr. Brad Richards. All rights reserved. This code
 * is licensed under the terms of the BSD 3-clause license (see the file
 * license.txt).
 * 
 * @author Brad Richards
 */
public class App_Controller extends Controller<App_Model, App_View> {
    ServiceLocator serviceLocator;
    
    
    private int clientID;
    

    public App_Controller(App_Model model, App_View view) {
        super(model, view);
        
        
        
      view.getBtnConnect().setOnAction(e-> this.connect());
      
      view.getExit().setOnAction(e->{
    	  Platform.exit();
      });
        
        
        
        
        
        
        
     // register ourselves to listen for button clicks
        view.btnClick.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                buttonClick();
            }
        });

        // register ourselves to handle window-closing event
        view.getStage().setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                Platform.exit();
            }
        });
        
        
        ChatRoomTeaserTrigger.getCRTT().addListener((ob, oldV, newV)->{
        	
        	//show new chatRoom
        	
        	
        	
        	
        });
        
        
        
        
        
        
        serviceLocator = ServiceLocator.getServiceLocator();        
        serviceLocator.getLogger().info("Application controller initialized");
    }
    
   

	public void buttonClick() {
        model.incrementValue();
        String newText = Integer.toString(model.getValue());        

        view.lblNumber.setText(newText);        
    }
	

	
	public void processChatRoomTrigger() {
		
		//showing chatRoom from client saved ones
		
	}
	
	
	
	
    
    
    public void processContainer(Container container) {
    	
    	String command = container.getCommand();
    	String userName = container.getUsername();
    	
    	Platform.runLater(()-> {
    	 switch(command) {
		
//		case "message":  model.getchatRoom.add(conatiner.getInformation, username) break;
		
		case "shutdown": System.out.println("Shutdown Command" + userName); break;
		
		case "firstContact": processFirstContact(container); break;
		
		
		
		
		}
    		
    		
    	});  
    	
    	
       
		
		
    }
    
    
    

    
    private void processFirstContact(Container container) {
	
    	this.processID(container);
    	this.processChatRooms(container);
    	this.processContact(container);
    	
    	
    	
		
	}



	private void processContact(Container container) {
	
		
		
		
		
	}



	private void processID(Container container) {
		
		
	this.clientID =	container.getID();
		
	
	}



	private void processChatRooms(Container container) {
		
		
		
		ArrayList<ChatRoomTeaser> teasers  = new ArrayList<>();
		
		ArrayList<String> t = container.getRooms();
		
		for(String r:t) {
			
		String[] s =	r.split(";");
			
			
			ChatRoomTeaser c = new ChatRoomTeaser(s[0], s[1]);
		}
		
		
		
		this.view.getChatroomOverView().populateHolder(teasers);
		
	}
     
	
	


	private void connect() {
    	
    	
    	int port;
    	String iP;
    	
    	
    	try {
    		port = Integer.parseInt(view.getPortInput().getText());
    		iP = view.getIpInput().getText();
    		
    		NetRecSend.getNRS().startClient(port, iP, this);
    		
    		
    		String userName = view.getUserNameInput().getText();
    	   Container c = new Container(userName, "brief");
    		
    	   
    	   this.serviceLocator.getLogger().info("sending server breifing request");
    	   
    	   NetRecSend.getNRS().send(c);
    		
    		
    	} catch (NumberFormatException nFE) {
    		
    	//show the user he made a mistake when typing the port
    		

    	}
    	
    	
    	
    	
    	
    	
	}
    
    
}
