package chatminiproject.appClasses;

import java.util.Locale;
import java.util.logging.Logger;

import chatminiproject.ServiceLocator;
import chatminiproject.abstractClasses.View;
import chatminiproject.appClasses.views.ChatroomOverView;
import chatminiproject.appClasses.views.LoginBoxView;
import chatminiproject.commonClasses.Translator;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Copyright 2015, FHNW, Prof. Dr. Brad Richards. All rights reserved. This code
 * is licensed under the terms of the BSD 3-clause license (see the file
 * license.txt).
 * 
 * @author Brad Richards
 */
public class App_View extends View<App_Model> {
    Menu menuFile;
    Menu menuFileLanguage;
    Menu menuHelp;
    
    Label lblNumber;
    Button btnClick;
    
    /**
     * 
     * 
     * @author pascal konezciny
     * 
     * 
     */
	private BorderPane startUp;
	private HBox btnBox;
	private VBox  msgBox;
	private LoginBoxView loginBox;
	private TextField  message;
	private Button  exit, sendMsg;
	private TextArea txtChat;
	
	private ChatroomOverView chatroomOverView;
	
	
	
	public App_View(Stage stage, App_Model model) {
        super(stage, model);
        ServiceLocator.getServiceLocator().getLogger().info("Application view initialized");
    }
	

	@Override
	protected Scene create_GUI() {
	    ServiceLocator sl = ServiceLocator.getServiceLocator();  
	    Logger logger = sl.getLogger();
	    
	    
	    MenuBar menuBar = new MenuBar();
	    menuFile = new Menu();
	    menuFileLanguage = new Menu();
	    menuFile.getItems().add(menuFileLanguage);
	    
       for (Locale locale : sl.getLocales()) {
           MenuItem language = new MenuItem(locale.getLanguage());
           menuFileLanguage.getItems().add(language);
           language.setOnAction( event -> {
				sl.getConfiguration().setLocalOption("Language", locale.getLanguage());
                sl.setTranslator(new Translator(locale.getLanguage()));
                updateTexts();
            });
        }
	    
        menuHelp = new Menu();
	    menuBar.getMenus().addAll(menuFile, menuHelp);
	    
	    BorderPane root = new BorderPane();
	    
	    loginBox = new LoginBoxView();
	    
	    startUp = new BorderPane();
//		headBox = new HBox(10);
//		userNameBox = new HBox(10);
//		serverBox = new HBox(10);
		btnBox = new HBox(10);
		
		
		msgBox = new VBox(10);
//		loginBox = new VBox(10);
		
//		heading =  new Label();
//		userName =  new Label();
//		ipLbl =  new Label();
//		portLbl =  new Label();
		
//		userNameInput = new TextField();
//		ipInput = new TextField();
//		portInput = new TextField();
		message = new TextField();
		
		
//		btnConnect = new Button ();
		
		sendMsg = new Button ();
		exit = new Button ();
		
		
		
		this.chatroomOverView = new ChatroomOverView();
		
		txtChat = new TextArea();
		txtChat.setEditable(false);
		txtChat.setMouseTransparent(true);
		txtChat.setFocusTraversable(false);
	    
	    
		
		
		root.setTop(menuBar);
		root.setLeft(chatroomOverView);
		
		/**
		 * 
		 * @author pascal
		 * 
		 */
		
//		headBox.getChildren().addAll(heading);
//		headBox.setAlignment(Pos.TOP_CENTER);
//		
//		userNameBox.getChildren().addAll(userName, userNameInput, btnConnect);
//		userNameBox.setAlignment(Pos.CENTER);
//		serverBox.getChildren().addAll(ipLbl, ipInput , portLbl, portInput);
//		serverBox.setAlignment(Pos.CENTER);
		
		/**
		 * Format Button Box
		 * 
		 * @author pascal
		 */
		btnBox.getChildren().addAll(sendMsg, exit);
		btnBox.setAlignment(Pos.CENTER);
		btnBox.setPadding(new Insets(10,10,10,10));
		
//		loginBox.getChildren().addAll(headBox, userNameBox, serverBox);
//		loginBox.setAlignment(Pos.CENTER);
		
		msgBox.getChildren().addAll(txtChat, message);
		msgBox.setAlignment(Pos.CENTER);
		msgBox.setPadding(new Insets(10,5,5,5));
		
		
		startUp.setTop(loginBox);
		
		startUp.setCenter(msgBox);
		startUp.setBottom(btnBox);
		
		
		

        root.setCenter(startUp);
        
        
        
        btnClick = new Button();
        btnClick.setMinWidth(200);
        //root.setBottom(btnClick);
        
        updateTexts();
		
        Scene scene = new Scene(root, 450, 500);
        scene.getStylesheets().add(
                getClass().getResource("app.css").toExternalForm());
        return scene;
	}
	
	   protected void updateTexts() {
	       Translator t = ServiceLocator.getServiceLocator().getTranslator();
	        
	        // The menu entries
	       menuFile.setText(t.getString("program.menu.file"));
	       menuFileLanguage.setText(t.getString("program.menu.file.language"));
           menuHelp.setText(t.getString("program.menu.help"));
	        
	        // Buttons
          this.loginBox.getBtnConnect().setText(t.getString("button.clickme"));
           exit.setText(t.getString("button.exit"));
           sendMsg.setText(t.getString("button.sendMsg"));
           
           
           	// Labels
           this.loginBox.getHeading().setText(t.getString("label.heading"));
           this.loginBox.getUserName().setText(t.getString("label.userName"));
           this.loginBox.getIpLbl().setText(t.getString("label.ipLbl"));
           this.loginBox.getPortInput().setText(t.getString("label.portLbl"));
           
           	//Textfield
           
           this.loginBox.getIpInput().setText(t.getString("textfield.ipInput"));
           this.loginBox.getPortInput().setText(t.getString("textfield.portInput"));
           message.setPromptText(t.getString("textfield.message"));
           
           
           
           
           
           //Stage
           
           stage.setTitle(t.getString("program.name"));
	    }

	public VBox getMsgBox() {
		return msgBox;
	}

	public void setMsgBox(VBox msgBox) {
		this.msgBox = msgBox;
	}

	public TextField getMessage() {
		return message;
	}

	public void setMessage(TextField message) {
		this.message = message;
	}

	public Button getSendMsg() {
		return sendMsg;
	}

	public void setSendMsg(Button sendMsg) {
		this.sendMsg = sendMsg;
	}

	public TextArea getTxtChat() {
		return txtChat;
	}

	public void setTxtChat(TextArea txtChat) {
		this.txtChat = txtChat;
	}

	public Menu getMenuFile() {
		return menuFile;
	}

	public void setMenuFile(Menu menuFile) {
		this.menuFile = menuFile;
	}

	public Menu getMenuFileLanguage() {
		return menuFileLanguage;
	}

	public void setMenuFileLanguage(Menu menuFileLanguage) {
		this.menuFileLanguage = menuFileLanguage;
	}

	public Menu getMenuHelp() {
		return menuHelp;
	}

	public void setMenuHelp(Menu menuHelp) {
		this.menuHelp = menuHelp;
	}

	public Label getLblNumber() {
		return lblNumber;
	}

	public void setLblNumber(Label lblNumber) {
		this.lblNumber = lblNumber;
	}

	public Button getBtnClick() {
		return btnClick;
	}

	public void setBtnClick(Button btnClick) {
		this.btnClick = btnClick;
	}

	public BorderPane getStartUp() {
		return startUp;
	}

	public void setStartUp(BorderPane startUp) {
		this.startUp = startUp;
	}


	public HBox getUserNameBox() {
		return loginBox.getUserNameBox();
	}

	public void setUserNameBox(HBox userNameBox) {
		this.loginBox.setUserNameBox(userNameBox);
	}

	public HBox getServerBox() {
		return this.loginBox.getServerBox();
	}

	public void setServerBox(HBox serverBox) {
		this.loginBox.setServerBox(serverBox);
	}

	public HBox getBtnBox() {
		return btnBox;
	}

	public void setBtnBox(HBox btnBox) {
		this.btnBox = btnBox;
	}

	public VBox getLoginBox() {
		return loginBox;
	}



	public Label getHeading() {
		return this.loginBox.getHeading();
	}

	public void setHeading(Label heading) {
		this.loginBox.setHeading(heading);
	}

	public Label getUserName() {
		return loginBox.getUserName();
	}

	

	public Label getIpLbl() {
		return loginBox.getIpLbl();
	}

	

	public Label getPortLbl() {
		return loginBox.getPortLbl();
	}

	

	public TextField getUserNameInput() {
		return loginBox.getUserNameInput();
	}



	public TextField getIpInput() {
		return loginBox.getIpInput();
	}

	

	public TextField getPortInput() {
		return loginBox.getPortInput();
	}



	public Button getBtnConnect() {
		return this.loginBox.getBtnConnect();
	}


	public Button getExit() {
		return exit;
	}

	public void setExit(Button exit) {
		this.exit = exit;
	}


	public ChatroomOverView getChatroomOverView() {
		return chatroomOverView;
	}


	public void setChatroomOverView(ChatroomOverView chatroomOverView) {
		this.chatroomOverView = chatroomOverView;
	}


	public void setLoginBox(LoginBoxView loginBox) {
		this.loginBox = loginBox;
	}


}