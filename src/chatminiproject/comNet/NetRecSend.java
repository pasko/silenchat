package chatminiproject.comNet;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;

import chatminiproject.ServiceLocator;
import chatminiproject.appClasses.App_Controller;

public class NetRecSend extends Thread {
	
	
 private static NetRecSend netRecSend;
 
 	private int port;
 	private String iP;
 	
 	private App_Controller controller;
 	
 	
    private Socket socket;
    
    private ObjectInputStream in;
    private ObjectOutputStream out;
    private ServiceLocator sL;
    

   private NetRecSend() {
	   
	   this.sL = ServiceLocator.getServiceLocator();
	   
	   
	   
   }


   public static NetRecSend getNRS() {
	   
	   if(netRecSend == null) {
		   netRecSend = new NetRecSend();
	   }
	   
	   return  netRecSend;
   }
   

   
   public void startClient(int port, String iP, App_Controller controller) {
	   
	   this.port = port;
	   this.iP = iP;
	   
	   this.controller = controller;
	   
	   //
	   
	   try {
		   
		   this.socket = new Socket(this.iP, this.port);
		   
		   this.out = new ObjectOutputStream(socket.getOutputStream());
		   this.in = new ObjectInputStream(socket.getInputStream());
		   
		   
		   this.setDaemon(true);	
		   this.start();
		   
		   
	   } catch(Exception e) {
		   e.printStackTrace();
	   }
	   
	   
	   //
	   
	   
	  
	   
	   
	   
   }
   
   
   
   public  synchronized void send(Container c) {
	   
	   
	   
	   
	   
	   
	   try {
		this.out.writeObject(c);
		
		this.sL.getLogger().info("sending: " + c.stringRep());
		
		
		
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
   }
   
   
   public void run() {
	
	while(true) {
		
		try {
			Container c = (Container) in.readObject();
			
			this.controller.processContainer(c);
			
			
		} catch(SocketException sE) {
			sE.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		
	}
	
	

	
}





}