package chatminiproject.comNet;

import java.io.Serializable;
import java.util.ArrayList;

import javafx.application.Platform;

public class Container implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	
	private String username;
	private String information;
	





	private String command;
	private String destination;
	
	
	
	private ArrayList<String> rooms;
	private ArrayList<String> chatHist;
	private int iD;
	private ArrayList<String> contacts;
	
	

	
	
	public Container () {
		

	}
	
	
	
	
	
	public Container (String userName, String command, ArrayList<String> rooms, ArrayList<String> chatHist,ArrayList<String> contacts, int iD) {
		this.username = userName;
		this.command = command;
		this.rooms = rooms;
		this.contacts = contacts;
		this.chatHist = chatHist;
		this.iD = iD;
	}
	
	
	public Container (String username, String command) {
		this.username = username;
		this.command = command;
	}
	
	public Container(String username,  String command, String information) {
		this.username = username;
		this.information = information;
		this.command = command;
		
	}

	public Container(String username,  String command, String information, String destination) {
		this.username = username;
		this.information = information;
		this.command = command;
		this.setDestination(destination);
		
	}



	public String getCommand() {
		return this.command;
	}
	
	public String getUsername() {
		return username;
	}


	public String getInfomration() {
		return information;
	
	}




	public String getDestination() {
		return destination;
	}




	public void setDestination(String destination) {
		this.destination = destination;
	}




	public int getID() {
	   return	this.iD;
		
	}




	public ArrayList<String> getRooms() {
		// TODO Auto-generated method stub
		return this.rooms;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	public String getInformation() {
		return information;
	}


	public ArrayList<String> getChatHist() {
		return chatHist;
	}


	public int getiD() {
		return iD;
	}


	public ArrayList<String> getContacts() {
		return contacts;
	}


	public String getChatRoomName() {
		// TODO Auto-generated method stub
		return this.getChatRoomName();
	}





	public String stringRep() {
		
		
		
		
		return  "username: " + this.username + " command: " + this.command + " information: " + this.information + " iD: " + this.iD + " destination: " + this.destination;
	}
	
	
	

	
	
}
